import unittest
from heapq import heappush,heappop
from random import shuffle


def get_median():

  minheap = []
  maxheap = []

  def calc_median():
    if len(minheap) == len(maxheap): return float(minheap[0]-maxheap[0])/2
    return minheap[0]

  def add(x):
    # push to the right heap
    if not (minheap or maxheap) or x > calc_median(): heappush(minheap,x)
    else: heappush(maxheap,-1*x)

    # maintain heap balance
    while len(minheap) > len(maxheap):
      heappush(maxheap,-1*heappop(minheap))
    while len(minheap) < len(maxheap):
      heappush(minheap,-1*heappop(maxheap))

    return calc_median()

  return add


class TestOnlineMedian(unittest.TestCase):

  def setUp(self):
    self.onlinemedian = get_median()

  def testMedianSorted(self):
    A = range(100)
    for i in A: self.assertEquals( self.onlinemedian(i), self.offlinemedian(A[:i+1]) )

  def testMedianRandom(self):
    A = range(100)
    shuffle(A)
    for a in A[:-1]: self.onlinemedian(a)
    self.assertEquals( self.onlinemedian(A[-1]), self.offlinemedian(A) )

  def offlinemedian(self,A):
    A = A[:]
    A.sort()
    n = len(A)
    if n%2: return A[n/2]
    return float(A[n/2-1] + A[n/2])/2

unittest.main()
