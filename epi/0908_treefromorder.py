def construct(preorder,inorder,l,r):
  if not preorder or l>=r or l<0 or r>len(inorder): return None
  root = preorder[0]
  del preorder[0]
  for mid,c in enumerate(inorder):
    if c==root:
      #print root, mid, l,r,inorder,preorder
      return (root, construct(preorder, inorder, l,mid), construct(preorder, inorder, mid+1,r))

'''
inorder = list("4251673")
preorder = list("1245367")
'''
inorder = list("123")
preorder = list("312")

print construct(preorder,inorder,0,len(inorder))
