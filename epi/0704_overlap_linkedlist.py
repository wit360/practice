import unittest

def ll_len(L):
  ans = 0
  head = L
  while head:
    ans+=1
    head = head[1]
  return ans

def find_overlap(L1,L2):
  p1 = ll_len(L1)
  p2 = ll_len(L2)
  d = abs(p1-p2)

  short_cur = L1 if p1<p2 else L2
  long_cur = L2 if p1<=p2 else L1

  for i in range(d): long_cur = long_cur[1]
  while short_cur and long_cur and short_cur!=long_cur:
    short_cur = short_cur[1]
    long_cur = long_cur[1]
  return short_cur[0] if short_cur and short_cur==long_cur else None


class TestOverlappedLinkedlist(unittest.TestCase):
  def testLength(self):
    l0 = None
    l1 = (1,(2,(3,(4,None))))
    l2 = ('hello',(2,(3,None)))
    l3 = (1,(2,(3,l2)))
    self.assertEquals( ll_len(l0), 0 )
    self.assertEquals( ll_len(l1), 4 )
    self.assertEquals( ll_len(l2), 3 )
    self.assertEquals( ll_len(l3), 6 )

  def testOverlap(self):
    l0 = None
    l1 = (1,(2,(3,(4,None))))
    l2 = ('hello',(2,(3,None)))
    l3 = (1,(2,(3,l2)))
    self.assertEquals( find_overlap(l2,l3), "hello" )
    self.assertEquals( find_overlap(l2,l1), None )
