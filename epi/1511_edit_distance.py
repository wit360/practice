import pprint as pp
import sys

def distance(A,B):
	table = [[0 for j in range(len(B))] for i in range(len(A))]

	if A[0]!=B[0]: table[0][0] = 1

	for i in range(1,len(A)):
		table[i][0] = table[i-1][0]
		if A[i]!=B[0]: table[i][0]+=1

	for j in range(1,len(B)):
		table[0][j] = table[0][j-1]
		if A[0]!=B[j]: table[0][j]+=1

	for i in range(1,len(A)):
		for j in range(1,len(B)):
			if A[i]!=B[j]: 
				table[i][j] = min(table[i-1][j-1],table[i-1][j],table[i][j-1]) + 1
			else:
				table[i][j] = min(table[i-1][j-1],table[i-1][j]+1,table[i][j-1]+1)

	for i in range(len(A)):
		pp.pprint(table[i])


distance(sys.argv[1], sys.argv[2])
