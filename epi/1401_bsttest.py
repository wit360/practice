class Node:
	def __init__(self,d,l,r): 
		self.data, self.left, self.right = d,l,r

def checkbst(node, lb=float("-inf"), ub=float("inf")):
	if node is None: return True
	print "-----------"
	print "lb:" + str(lb)
	print "ub:" + str(ub)
	print "checking:" + str(node.data)
	if lb<node.data<ub:
		return checkbst(node.left,lb,node.data) and checkbst(node.right,node.data,ub)
	else:
		return False


tree1 = Node(6,Node(4,None,Node(5,None,None)),Node(10,Node(7,None,None),None))
print checkbst(Node(10,tree1,None))
