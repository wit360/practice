from heapq import heappush,heappop

def printk(A,k):
  print A,k
  heap = []
  for i in range(min(k,len(A))): heappush(heap,A[i])
  for i in range(k,len(A)):
      print heappop(heap)
      heappush(heap,A[i])
  while heap: print heappop(heap)
  pass


printk([3,2,1,6,5,4,9,8,7],3)
printk(range(1,7),3)
printk(range(6,0,-1),6)
printk([],3)
printk([2],3)
printk([2,1],3)
printk([2,4,1],3)

