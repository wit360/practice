import sys,unittest
from cStringIO import StringIO


def powerset(S,out=sys.stdout):
  N = 2**len(S)
  for i in range(N):
    s = []
    for j in range(len(S)):
      if 2**j & i != 0:
        s.append(S[j])
    if s:
      out.write(", ".join(s) + "\n")
    else:
      out.write("(empty set)\n")
  out.flush()


class TestPowerset(unittest.TestCase):
  def setUp(self):
    self.out = StringIO()

  def testEmpty(self):
    powerset([], self.out)
    self.assertEquals( self.out.getvalue() , "(empty set)\n" ) 

  def testABPowerset(self):
    powerset(list("AB"), self.out)
    expected = [
                "(empty set)",
                "A",
                "B",
                "A, B",
                ""
               ]
    self.assertEquals( self.out.getvalue() , "\n".join(expected) )
